# Diplômés 2015 de la Villa Arson

Every year, the students of the art school of la Villa Arson, in Nice, France, show their work in a collective exhibition.
A website comes along the exhibition to be used as a portfolio for these young artists who don't necessarily have a website yet.
The website has been developed in Wordpress internally to the school with some graphical advices from OSP.
OSP has then branched the Wordpress to the html2print boilerplate to design PDFs for each of the student and one gathering all the works.

<http://diplomes2015.villa-arson.org>

Every visitor can print the PDF by itself by using the Print dialog of their web browser (tested on Chrome, Firefox, Safari, Epiphany).

Layout specificities:
    - The printed version keeps notion of the color gradient: each work has one color of the gradient.
    - Use of flexbox to layout elements.
    - When there's only one image and no text, the image uses the whole page.

